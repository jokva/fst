#ifndef FIXED_STRING_FST_HPP
#define FIXED_STRING_FST_HPP

#include <algorithm>
#include <array>
#include <functional>
#include <ostream>
#include <string>

namespace fst {

namespace {

/* Providing a custom min() because std::min dangles if passed an rvalue that
 * gets selected. Since this will only be used for integral types, copying
 * might also be faster.
 */
template< typename T, typename U >
constexpr inline auto min( T t, U u ) noexcept -> decltype( t + u ) {
    return t < u ? t : u;
}

}

template< typename Str >
struct pad_auto_trunc {
    static auto uppercase( typename Str::value_type ch ) -> decltype( ch ) {
        return std::toupper( ch );
    }

    template< typename InputIt >
    static Str& assign( InputIt first, InputIt last, Str& str ) noexcept {
        const auto n = min( std::distance( first, last ), Str::len );
        auto end = std::copy_n( first, n, str.buffer.begin() );
        std::transform( first, first + n, str.buffer.begin(), uppercase );
        std::fill( end, str.buffer.end(), Str::pad );
        return str;
    }

    static Str& assign( typename Str::size_type count,
                        typename Str::value_type ch,
                        Str& str ) noexcept {
        const auto n = min( count, Str::len );
        auto itr = str.buffer.begin();
        auto end = std::fill_n( itr, n, ch );
        std::transform( itr, itr + n, str.buffer.begin(), uppercase );
        std::fill( end, str.buffer.end(), Str::pad );
        return str;
    }

    constexpr static
    auto size( const Str& str ) noexcept -> decltype( str.buffer.max_size() ) {
        return str.buffer.max_size();
    }

    static typename Str::iterator erase( typename Str::const_iterator first,
                                         typename Str::const_iterator last,
                                         Str& str ) noexcept {
        const auto fst_index = std::distance( str.cbegin(), first );
        const auto lst_index = std::distance( str.cbegin(), last );
        auto fst = str.begin() + min( fst_index, Str::len );
        auto lst = str.begin() + min( lst_index, Str::len );
        std::fill( fst, lst, Str::pad );
        return { lst };
    }

    template< typename InputIt >
    static typename Str::iterator insert( typename Str::const_iterator pos,
                                          InputIt first,
                                          InputIt last,
                                          Str& str ) {
    }
};

/*
 * basic_string expects a struct with the following operations:
 *
 *   template< typename InputIt >
 *   basic_string& assign( InputIt first, InputIt last, Str& str )
 *   basic_string& assign( size_type, value_type, Str& str )
 *   size_type size( Str& str ) const noexcept
 *   bool empty( Str& str ) const noexcept
 *   iterator erase( const_iterator first, const_iterator last )
 *   template< typename InputIt >
 *   void append( InputIt first, InputIt last, Str& str )
 *   reference back( Str& str )
 *
 * All other methods are derived from the implementations of these methods.
 * They're used to control things like truncating, exceptions, padding
 * behaviour and the notion of size, emptiness, and equality.
 *
 * The struct will be given basic_string itself as a template, meaning it has
 * access to all the definitions (and the underlying buffer) - it will be
 * friended. If the methods are marked with noexcept the corresponding methods
 * in basic_string will be marked noexcept.
 */

template< unsigned Len,
          typename CharT = char,
          CharT Pad = ' ',
          template< typename > class Ops = pad_auto_trunc,
          typename Traits = std::char_traits< CharT > >
class basic_string {

    using Operation = Ops< basic_string >;
    using storage = std::array< CharT, Len >;

    static constexpr auto assign_noexcept = noexcept( Operation::assign );
    static constexpr auto append_noexcept = noexcept( Operation::append );
    static constexpr auto erase_noexcept  = noexcept( Operation::erase );
    static constexpr auto swap_noexcept   = noexcept( storage::swap );

    public:

        static constexpr unsigned len { Len };
        static constexpr CharT    pad { Pad };
        using traits_type     = Traits;
        using value_type      = typename Traits::char_type;
        using reference       = value_type&;
        using const_reference = const value_type&;
        using pointer         = value_type*;
        using const_pointer   = const value_type*;
        using difference_type = typename storage::difference_type;
        using size_type       = typename storage::size_type;
        using iterator        = typename storage::iterator;
        using const_iterator  = typename storage::const_iterator;

        /* constructor */
        basic_string() noexcept( assign_noexcept ) :
            basic_string( len, pad ) {}

        basic_string( size_type count, CharT ch ) noexcept( assign_noexcept ) {
            this->assign( count, ch );
        }

        basic_string( const basic_string& str, size_type pos )
            noexcept( assign_noexcept ) :
        basic_string( str, pos, len - pos ) {}

        basic_string( const basic_string& str,
                      size_type pos,
                      size_type count ) noexcept( assign_noexcept ) :
            basic_string( str.buffer.begin() + pos,
                          str.buffer.begin() + pos + count ) {}

        basic_string( const std::string& str ) noexcept( assign_noexcept ) :
            basic_string( str, 0 ) {}

        basic_string( const std::string& str, size_type pos )
        noexcept( assign_noexcept ) :
            basic_string( str, pos, str.size() ) {}

        basic_string( const std::string& str,
                      size_type pos,
                      size_type count ) noexcept( assign_noexcept ) :
        basic_string( str.data() + pos, str.data() + pos + count ) {}

        basic_string( const CharT* s ) noexcept( assign_noexcept ) {
            this->assign( s );
        }

        basic_string( const CharT* s, size_type count )
        noexcept( assign_noexcept ) :
            basic_string( s, s + count ) {}

        template< typename InputIt >
        basic_string( InputIt first, InputIt last )
        noexcept( assign_noexcept ) {
            this->assign( first, last );
        }

        basic_string( std::initializer_list< CharT > ilist )
        noexcept( assign_noexcept ) :
            basic_string( ilist.begin(), ilist.end() ) {}

        /* assign */
        basic_string& assign( size_type count, CharT ch )
        noexcept( assign_noexcept ) {
            return Operation::assign( count, ch, *this );
        }

        basic_string& assign( const basic_string& str )
        noexcept( assign_noexcept ) {
            return this->assign( str.begin(), str.end() );
        }

        basic_string& assign( const basic_string& str,
                              size_type pos,
                              size_type count = len )
        noexcept( assign_noexcept ) {
            return this->assign( str.data() + pos, str.data() + pos + count );
        }

        basic_string& assign( basic_string&& str ) noexcept( assign_noexcept ) {
            return this->assign( str );
        }

        basic_string& assign( const std::string& str )
        noexcept( assign_noexcept ) {
            return this->assign( str.begin(), str.end() );
        }

        basic_string& assign( const std::string& str,
                              size_type pos,
                              size_type count = len )
        noexcept( assign_noexcept ) {
            return this->assign( str.begin() + pos, str.begin() + pos + count );
        }

        basic_string& assign( const CharT* s, size_type count )
        noexcept( assign_noexcept ) {
            return this->assign( s, s + min( count, Traits::length( s ) ) );
        }

        basic_string& assign( const CharT* s ) noexcept( assign_noexcept ) {
            return this->assign( s, min( len, Traits::length( s ) ) );
        }

        template< typename InputIt >
        basic_string& assign( InputIt first, InputIt last )
        noexcept( assign_noexcept ) {
            return Operation::assign( first, last, *this );
        }

        basic_string& assign( std::initializer_list< CharT > ilist )
        noexcept( assign_noexcept ) {
            return this->assign( ilist.begin(), ilist.end() );
        }

        /* at, operator[], front, back */
        reference at( size_type pos ) {
            return this->buffer.at( pos );
        }

        const_reference at( size_type pos ) const {
            return this->buffer.at( pos );
        }

        reference operator[]( size_type pos ) noexcept {
            static CharT nullchar = CharT();
            if( pos == this->buffer.max_size() ) return nullchar;
            return this->buffer[ pos ];
        }

        const_reference operator[]( size_type pos ) const noexcept {
            static const CharT nullchar = CharT();
            if( pos == this->buffer.max_size() ) return nullchar;
            return this->buffer[ pos ];
        }

        reference front() noexcept {
            return this->buffer.front();
        }

        const_reference front() const noexcept {
            return this->buffer.front();
        }

        /* empty */
        bool empty() const noexcept {
            return Operation::empty( *this );
        }

        /* size */
        size_type size() const noexcept {
            return Operation::size( *this );
        }

        size_type length() const noexcept {
            return this->size();
        }

        constexpr size_type max_size() const noexcept {
            return len;
        }

        /* reserve, capacity and shrink_to_fit */
        /* these operations are nonsensical on a fixed-size string, but are
         * included for completeness. reserve is a no-op unless called with
         * new_cap > max_size(), in which case it will throw, to not break
         * programs that relies on reserving-and-checking exceptions.
         */

        void reserve( size_type new_cap = 0 ) {
            if( new_cap > this->max_size() ) throw std::length_error(
                "Reserving capacity larger than max_size (which is " +
                std::to_string( this->max_size() ) + "). " +
                "This operation is nonsensical for fst::basic_string" );
        }

        constexpr size_type capacity() const noexcept {
            return this->max_size();
        }

        void shrink_to_fit() noexcept {}

        /* clear */
        void clear() noexcept( erase_noexcept ) {
            this->erase( this->begin(), this->end() );
        }

        /* erase */
        basic_string& erase( size_type index = 0, size_type count = len )
        noexcept( erase_noexcept ) {
            const auto rindex = min( index, len );
            this->erase( this->buffer.cbegin() + rindex,
                         this->buffer.cbegin() + min( rindex + count, len ) );
            return *this;
        }

        iterator erase( const_iterator pos ) noexcept( erase_noexcept ) {
            return this->erase( pos, this->end() );
        }

        iterator erase( const_iterator first, const_iterator last )
        noexcept( erase_noexcept ) {
            return Operation::erase( first, last, *this );
        }

        /* append, push_back */
        basic_string& append( size_type count, CharT ch )
        noexcept( append_noexcept ) {
        }

        basic_string& append( const basic_string& str )
        noexcept( append_noexcept ) {
            return this->append( str.begin(), str.end() );
        }

        basic_string& append( const std::string& str )
        noexcept( append_noexcept ) {
            return this->append( str.begin(), str.end() );
        }

        basic_string& append( const basic_string& str,
                              size_type pos,
                              size_type count = len )
        noexcept( append_noexcept ) {
            return this->append( str.begin() + pos, str.begin() + pos + count );
        }

        basic_string& append( const CharT* str,
                              size_type count ) noexcept( append_noexcept ) {
            return this->append( str, str + count );
        }

        basic_string& append( const CharT* str ) noexcept( append_noexcept ) {
            return this->append( str, str + min( Traits::length( str ), len ) );
        }

        basic_string& append( std::initializer_list< CharT > ilist )
        noexcept( append_noexcept ) {
            return this->append( ilist.begin(), ilist.end() );
        }

        template< typename InputIt >
        basic_string& append( InputIt first, InputIt last )
        noexcept( append_noexcept ) {
            this->insert( Operation::find_end( *this ), first, last );
            return *this;
        }

        void push_back( CharT ch ) noexcept( append_noexcept ) {
            this->append( 1, ch );
        }

        /* operator += */
        basic_string& operator+=( const basic_string& str )
        noexcept( append_noexcept ) {
            return this->append( str );
        }

        basic_string& operator+=( const std::string& str )
        noexcept( append_noexcept ) {
            return this->append( str );
        }

        basic_string& operator+=( CharT ch ) noexcept( append_noexcept ) {
            return this->append( 1, ch );
        }

        basic_string& operator+=( const CharT* s )
        noexcept( append_noexcept ) {
            return this->append( s );
        }

        basic_string& operator+=( std::initializer_list< CharT > ilist )
        noexcept( append_noexcept ) {
            return this->append( ilist );
        }

        /* compare */
        int compare( const basic_string& str ) const noexcept {
            return Traits::compare( this->data(), str.data(), len );
        }

        int compare( size_type pos1,
                     size_type count1,
                     const basic_string& str ) const {
            basic_string str1( str, pos1, count1 );
            return str1.compare( str );
        }

        int compare( size_type pos1,
                     size_type count1,
                     const basic_string& str,
                     size_type pos2,
                     size_type count2 = len ) const {
            basic_string str1( *this, pos1, count1 );
            basic_string str2( str, pos2, count2 );
            return str1.compare( str2 );
        }

        int compare( const std::string& str ) const noexcept {
            basic_string other( str );
            return this->compare( other );
        }

        int compare( size_type pos1,
                     size_type count1,
                     const std::string& str ) const {
            basic_string str1( str, pos1, count1 );
            return str1.compare( str );
        }

        int compare( size_type pos1,
                     size_type count1,
                     const std::string& str,
                     size_type pos2,
                     size_type count2 = len ) const {
            basic_string str1( *this, pos1, count1 );
            basic_string str2( str, pos2, count2 );
            return str1.compare( str2 );
        }

        int compare( const CharT* s ) const noexcept {
            basic_string other( s );
            return this->compare( other );
        }

        int compare( size_type pos1,
                     size_type count1,
                     const CharT* s ) const {
            basic_string str1( *this, pos1, count1 );
            return str1.compare( s );
        }

        int compare( size_type pos1,
                     size_type count1,
                     const CharT* s,
                     size_type count2 ) const {
            basic_string str1( *this, pos1, count1 );
            basic_string str2( s, count2 );
            return str1.compare( str2 );
        }

        basic_string substr( size_type pos = 0, size_type count = len ) const {
            return basic_string( *this, pos, count );
        }

        size_type copy( CharT* dest,
                        size_type count,
                        size_type pos = 0 ) const {
            const auto fin = std::min( len, pos + count );
            std::copy( this->begin() + pos, this->begin() + fin, dest );
            return fin - pos;
        }

        void swap( basic_string& other ) noexcept( swap_noexcept ) {
            this->buffer.swap( other.buffer );
        }

        /* data */
        pointer data() noexcept                { return this->buffer.data(); }
        const_pointer data() const noexcept    { return this->buffer.data(); }

        /* iterators, begin/end */
        iterator begin() noexcept              { return this->buffer.begin(); }
        iterator end() noexcept                { return this->buffer.end(); }
        const_iterator begin() const noexcept  { return this->buffer.begin(); }
        const_iterator end() const noexcept    { return this->buffer.end(); }
        const_iterator cbegin() const noexcept { return this->buffer.begin(); }
        const_iterator cend() const noexcept   { return this->buffer.end(); }

        bool operator==( const basic_string& rhs ) const {
            return std::equal( rhs.begin(), rhs.end(), this->begin() );
        }

        template< typename T >
        bool operator==( const T& rhs ) const {
            basic_string other( rhs );
            return this->operator==( other );
        }

        template< typename T >
        bool operator!=( const T& rhs ) const {
            return !( this->operator==( rhs ) );
        }

        template< typename T >
        bool operator<( const T& rhs ) const {
            basic_string other( rhs );
            return this->compare( other ) < 0;
        }

        template< typename T >
        bool operator>( const T& rhs ) const {
            basic_string other( rhs );
            return this->compare( other ) > 0;
        }

    private:
        storage buffer;
        friend class Ops< basic_string >;

        friend std::ostream& operator<<( std::ostream& stream,
                                         const basic_string& str ) {
            return stream.write( str.data(), len );
        }

        template< typename T >
        friend bool operator==( const T& lhs, const basic_string& rhs ) {
            return rhs.operator==( lhs );
        }

        template< typename T >
        friend bool operator!=( const T& lhs, const basic_string& rhs ) {
            return rhs.operator!=( lhs );
        }

        template< typename T >
        friend bool operator<( const T& lhs, const basic_string& rhs ) {
            basic_string other( lhs );
            return other.operator<( rhs );
        }

        template< typename T >
        friend bool operator>( const T& lhs, const basic_string& rhs ) {
            basic_string other( lhs );
            return other.operator>( rhs );
        }
};

template< unsigned Len,
          typename CharT,
          CharT Pad,
          template< typename > class Ops,
          typename Traits >
constexpr unsigned basic_string< Len, CharT, Pad, Ops, Traits >::len;

template< unsigned Len,
          typename CharT,
          CharT Pad,
          template< typename > class Ops,
          typename Traits >
constexpr CharT basic_string< Len, CharT, Pad, Ops, Traits >::pad;

}

namespace std {

template<>
template< unsigned Len,
          typename CharT,
          CharT Pad,
          template< typename > class Ops,
          typename Traits >
struct hash< fst::basic_string< Len, CharT, Pad, Ops, Traits > > {
    using argument_type = fst::basic_string< Len, CharT, Pad, Ops, Traits >;
    std::size_t operator()( const argument_type& s ) const noexcept {
        std::size_t res = 0;
        std::hash< CharT > hashfn;
        for( auto ch : s ) {
            res ^= hashfn( ch ) + 0x9e3779b9 + ( res << 6 ) + ( res >> 2 );
        }

        return res;
    }
};

}

#endif //FIXED_STRING_FST_HPP
