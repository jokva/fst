#include <cassert>
#include <iostream>

#include <fst.hpp>

void string8() {
    using string = fst::basic_string< 8 >;

    /* constructors */
    assert( "        " == string() );
    assert( "        " == string( 0, 'a' ) );
    assert( "AAAAAAAA" == string( 8, 'a' ) );

    const string str( 4, 'a' );
    assert( "AA      " == string( str, 2 ) );
    assert( "AA      " == string( str, 1, 2 ) );
    assert( "AAA     " == string( str, 1 ) );

    assert( "LITERAL " == string( "LITERAL" ) );
    assert( "LITERAL " == string( "LITERAL", 7 ) );
    assert( "LIT     " == string( "LITERAL", 3 ) );
    assert( "LIT     " == string( "LIT\0ERAL" ) );
    assert( "LITERAL_" == string( "LITERAL_LONG" ) );
    assert( "L       " == string( "LITERAL_LONG", 1 ) );

    std::string stdstr( "standard string" );
    assert( "STANDARD" == string( stdstr ) );
    assert( "ANDARD S" == string( stdstr, 2 ) );
    assert( "AND     " == string( stdstr, 2, 3 ) );
    assert( "TANDARD " == string( stdstr, 1, 40 ) );

    assert( "INITIALI" == ( string { "initiali" } ) );
    assert( "INITIALI" == ( string { "initializer" } ) );
    assert( "INIT    " == ( string { "init" } ) );
    assert( "        " == ( string { "" } ) );

    std::string itrstr( "ITERATOR STRING" );
    assert( "ITERATOR" == string( itrstr.begin(), itrstr.end() ) );
    assert( "ITER    " == string( itrstr.begin(), itrstr.begin() + 4 ) );

    string op_assign1 = string{ "INITIALI" };
    string op_assign2 = std::move( string{ "INITIALI" } );
    string op_assign3 = "INITIALI";

    assert( "INITIALI" == op_assign1 );
    assert( "INITIALI" == op_assign2 );
    assert( "INITIALI" == op_assign3 );

    /* assign */
    assert( "        " == string().assign( 0, 'a' ) );
    assert( "AAAAAAAA" == string().assign( 8, 'a' ) );
    assert( "AAAAAAAA" == string().assign( 10, 'a' ) );
    assert( "INITIALI" == string().assign( op_assign1 ) );
    assert( "STANDARD" == string().assign( stdstr ) );
    assert( "ANDARD S" == string().assign( stdstr, 2 ) );
    assert( "AND     " == string().assign( stdstr, 2, 3 ) );
    assert( "TANDARD " == string().assign( stdstr, 1, 40 ) );
    assert( "CSTR    " == string().assign( "CSTRING0", 4 ) );
    assert( "CSTRING0" == string().assign( "CSTRING0", 10 ) );
    assert( "CSTRING0" == string().assign( "CSTRING0" ) );
    assert( "ITERATOR" == string().assign( itrstr.begin(), itrstr.end() ) );
    assert( "ITER    " == string().assign( itrstr.begin(), itrstr.begin() + 4 ) );

    /* operator[], at(), front */
    assert( string( "literal" ).at( 0 ) == 'L' );
    assert( string( "literal" ).at( 1 ) == 'I' );
    assert( str.at( 0 ) == 'A' );
    assert( str.at( 4 ) == ' ' );
    assert( string( "literal" )[ 0 ] == 'L' );
    assert( string( "literal" )[ 1 ] == 'I' );
    assert( string( "literal" )[ 8 ] == '\0' );
    assert( str[ 0 ] == 'A' );
    assert( str[ 4 ] == ' ' );
    assert( str[ 8 ] == '\0' );
    assert( str.front() == str.at( 0 ) );
    assert( op_assign1.front() == op_assign1.at( 0 ) );

    /* clear, erase */
    string clear( str );
    string erase1( itrstr );
    string erase2( itrstr );
    erase1.erase( erase1.begin() + 4 );
    erase2.erase( erase2.begin() + 2, erase2.begin() + 4 );

    clear.clear();
    assert( "        " == clear );
    assert( "        " == string( stdstr ).erase() );
    assert( "        " == string( stdstr ).erase() );
    assert( "ITER    " == string( itrstr ).erase( 4 ) );
    assert( "IT  ATOR" == string( itrstr ).erase( 2, 2 ) );
    assert( "  ERATOR" == string( itrstr ).erase( 0, 2 ) );
    assert( "ITER    " == erase1 );
    assert( "IT  ATOR" == erase2 );

    assert( 8 == string().size() );
    assert( 8 == string().length() );
    assert( 8 == string().max_size() );
    assert( 8 == string().capacity() );
    string().reserve( 8 );
    string().reserve( 7 );
    string().shrink_to_fit();
}

int main() {
    string8();
}
